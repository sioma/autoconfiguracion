#!/bin/bash
g="\033[0;32m"
n="\033[0m"
echo -e "${g}Habilitando SSH..........................${n}"
sudo systemctl enable ssh
echo -e "${g}Instalando postgresql....................${n}"
sudo apt-get -y update
sudo apt-get -y purge postgresql*
sudo apt-get -f install
sudo apt-get -y install postgresql
echo -e "${g}Instalando Psycopg2......................${n}"
sudo apt-get -y install python-psycopg2
echo -e "${g}Instalando libreria de 7-segmentos.......${n}"
sudo apt-get -y install build-essential python-dev python-smbus python-imaging git
git clone https://github.com/adafruit/Adafruit_Python_LED_Backpack.git
sudo python Adafruit_Python_LED_Backpack/setup.py install
echo -e "${g}Instalando rquests.......................${n}"
sudo apt-get install python-requests
echo -e "${g}Creando base de datos local..............${n}"
sudo -u postgres -p sioma psql -c "CREATE DATABASE estomadb;"
echo -e "${g}Base de datos creada.....................${n}"
echo -e "${g}Cambiando contraseña Postgres............${n}"
sudo -u postgres psql -c "ALTER USER postgres WITH PASSWORD 'sioma';"
echo -e "${g}Actualizando autenticacion de postgres...${n}"
sudo sed -i -e "s/peer/md5/g" /etc/postgresql/11/main/pg_hba.conf
sudo service postgresql restart
echo -e "${g}Instalando i2c-tools.....................${n}"
sudo apt-get -y install i2c-tools
echo -e "${g}Localizando RTC..........................${n}"
sudo i2cdetect -y 1
echo -e "${g}Agregando RTC............................${n}"
sudo echo "dtoverlay=i2c-rtc,ds3231" >> /boot/config.txt
sudo echo "rtc-ds3231" >> /etc/modules
sudo sed -i '7,9 s/^/#/' /lib/udev/hwclock-set
echo -e "${g}Actualizando hora de RTC.................${n}"
sudo hwclock -w
echo -e "${g}Hora actual del RTC:${n}"
sudo hwclock -r
echo -e "${g}Estatus de clock:${n}"
timedatectl status
echo -e "${g}Descargando codigos......................${n}"
dir=$(pwd)
cd /
sudo mkdir firmware
cd firmware
sudo mkdir  base
cd base
sudo mkdir Codes
cd Codes
sudo git clone https://cristian13r:MHqfm9Sm6nPMN9yZEMmD@bitbucket.org/sioma/initbascula.git
echo -e "${g}Creando autorun..........................${n}"
sudo cp $dir"/autorun" /etc/init.d/autorun
sudo chmod 755 /etc/init.d/autorun
sudo update-rc.d autorun defaults
sudo update-rc.d autorun disable 2
sudo apt-get -y install chkconfig
sudo chkconfig -list
echo -e "${g}Creando autorun..........................${n}"
sudo apt-get purge bluez -y
sudo apt-get autoremove -y
echo -e "${g}Creando carpeta de backup..........................${n}"
sudo mkdir /home/pi/datos
sudo mkdir /home/pi/datos/db
sudo mkdir /home/pi/datos/logData
sudo chown pi /home/pi/datos -R
sudo chown postgres /home/pi/datos/db
echo -e "${g}Instalando Apache..........................${n}"
sudo apt-get install apache2 -y
sudo a2enmod rewrite
echo -e "${g}Instalando php..........................${n}"
sudo apt-get install php -y
sudo apt-get install php-pgsql -y
#echo -e "${g}Descargando Servidor..........................${n}"
#cd /var/www/html
#sudo git clone https://cristian13r:MHqfm9Sm6nPMN9yZEMmD@bitbucket.org/sioma/nukakapp.git
sudo sed -i '12 s+/html+/html/nukakapp+' /etc/apache2/sites-available/000-default.conf
sudo service apache2 restart
echo -e "${g}Instalando paquetes para Wifi..........................${n}"
##Configuracion Manual Realice los pasos  siguiendo la documentacion de CONFIGURACION ADAPTADOR WIFI A RASPBERRY
sudo sed '20 a www-data ALL=(ALL) NOPASSWD:ALL' -i /etc/sudoers
sudo chmod 755 -R /var/www/html/nukakapp/
sudo chown -R www-data:www-data /var/www/html/nukakapp/
echo -e "${g}Configuracion finalizada, Esta Raspberry ahora es una Nukak ${n}"
